import { createBrowserRouter } from "react-router-dom";
import { FlightsPage } from "../features/flights/page/FlightsPage";

export const router = createBrowserRouter([
    {
      path: "/flights",
      element: <FlightsPage />,
    },
  ]);