import { useQuery } from "@tanstack/react-query";
import { FlightRepository } from "../data/FlightRepository";
import { Flight } from "../models/Flight";

export const useFetchFlights = () => {
  //TODO:
  console.log('useFetchFlights body')
  const repository =  FlightRepository.getInstance()
  const { data, isLoading, isError, isSuccess } = useQuery<Flight[]>({
    queryKey: ["flights"],
    queryFn: () => repository.getFlights(),
  });

  return {
    flights: data,
    isFetchFlightsLoading: isLoading,
    isFetchFlightsSuccess: isSuccess,
    isFetchFlightsError: isError,
    isError:isError,
  };
};
