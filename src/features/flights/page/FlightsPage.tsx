import { Flights } from "../components/Flights";
import { useFetchFlights } from "../UseCases/useFetchFlights";
import { LoadPanel } from 'devextreme-react/load-panel';
import { Flight } from "../models/Flight";
export function FlightsPage() {
  const { flights, isFetchFlightsLoading, isFetchFlightsSuccess, isError } =
    useFetchFlights();
  return (
    <div style={{ backgroundColor: "white", height: "100vh" }}>
      <LoadPanel
        shadingColor="rgba(0,0,0,0.4)"
        //position={position}
        visible={isFetchFlightsLoading}
        showIndicator={true}
        shading={true}
        showPane={true}
        hideOnOutsideClick={false}
      />
      {isError && <div>Something went wrong</div>}
      {/*TODO : whene I put as Flight[], flights still of type flights: Flight[] | undefined how can I force*/}
      {isFetchFlightsSuccess && <Flights flights={flights as Flight[]} />}
    </div>
  );
}
