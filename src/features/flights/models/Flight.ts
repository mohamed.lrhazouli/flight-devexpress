export interface Flight {
  iataAirlineCode: string;
  flightNumber: number;
  flightDate: string;
  origin: string;
  destination: string;
  contingents: Contingent[];
  FN:string;
  FI:string;

}

interface Contingent {
  clientCode: string;
  totalSeats: number;
  bookedSeats: number;
}