import { Flight } from "../models/Flight";
import { FlightDataSource } from "./FlightAPIDataSource";

interface FlightRepositoryInterface {
  getFlights(): Promise<Flight[]>;
}

export class FlightRepository implements FlightRepositoryInterface {
  datasource: FlightDataSource = FlightDataSource.getInstance();
  private static instance: FlightRepository;
  public static getInstance(): FlightRepository {
    if (!FlightRepository.instance) {
      FlightRepository.instance = new FlightRepository();
    }
    return FlightRepository.instance;
  }
  private constructor() {
    console.log("Instancing FlightRepository");
  }
  async getFlights(): Promise<Flight[]> {
    return await this.datasource.getFlights();
  }
}
