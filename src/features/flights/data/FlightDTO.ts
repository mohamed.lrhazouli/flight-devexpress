interface Contingent {
  clientCode: string;
  totalSeats: number;
  bookedSeats: number;
}

export interface FlightDTO {
  flightId: number;
  iataAirlineCode: string;
  flightNumber: number;
  flightDate: string;
  origin: string;
  destination: string;
  contingents: Contingent[];
}
