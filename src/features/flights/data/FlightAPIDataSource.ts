import { Http } from "../../../api/Http";
import { Flight } from "../models/Flight";
import { FlightDTO } from "./FlightDTO";

interface FlightDataSourceInterface {
  getFlights(): Promise<Flight[]>;
}

export class FlightDataSource implements FlightDataSourceInterface {
  private static instance: FlightDataSource;

  private constructor() {
    console.log("Instancing FlightDataSource");
  }
  public static getInstance(): FlightDataSource {
    if (!FlightDataSource.instance) {
      FlightDataSource.instance = new FlightDataSource();
    }
    return FlightDataSource.instance;
  }
  async getFlights(): Promise<Flight[]> {
    const res = await Http.get<FlightDTO[]>("/flights");
    return res.data.map((x: FlightDTO) => this.mapToModel(x));
  }

  mapToModel(flight: FlightDTO): Flight {
    return {
      ...flight,

      FN: `${flight.iataAirlineCode} ${flight.flightNumber}`,
      FI: `${flight.flightDate} ${flight.origin} - ${flight.destination}`,
    };
  }
}
