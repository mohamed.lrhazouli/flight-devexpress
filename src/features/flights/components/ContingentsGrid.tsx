import { DataGrid, Item, Toolbar } from "devextreme-react/data-grid";
import React from "react";
import { Flight } from "../models/Flight";

const onCellPrepared = (e: any) => {
  e.cellElement.className = "thick";
  if (e.rowType === "data" && e.column.dataField === "bookedSeats") {
    console.log(e); //this log is very interesting
    let percent = e.data.bookedSeats / e.data.totalSeats;
    //233d6e
    e.cellElement.style.backgroundColor =
      percent >= 0.8 ? "#ffe1ea" : percent >= 0.5 ? "#e1ffe7" : "#e1e9ff";
  }
};
interface Props{
  flight: Flight | undefined
}
const ContingentsGrid: React.FC<Props> = ({flight}) => {
  //We can preview the first flight as the default previewd one
  //insted of showing the message "Select a flight to preview its contigents"
  return (
    <div>
      {flight ? (
        <DataGrid
          showBorders={true}
          id="dataGrid"
          dataSource={flight.contingents} //this is where the data comes from
          keyExpr="clientCode" //???
          allowColumnResizing={false} //allow user to resize columns
          columnAutoWidth={true}
          allowColumnReordering={false}
          onCellPrepared={onCellPrepared}
          height={"100%"}
          className="contigent-grid"
          showColumnLines={true}
          showRowLines={true}
        >
          <Toolbar>
            <Item location="before">
              <span className="thick">{`${flight.FN} | ${flight.FI}`}</span>
            </Item>

            <Item name="applyFilterButton" />

            <Item name="exportButton" />

            <Item location="before" name="searchPanel" />
          </Toolbar>
        </DataGrid>
      ) : (
        <div>Select a flight to preview its contigents</div>
      )}
    </div>
  );
};

export default ContingentsGrid;
