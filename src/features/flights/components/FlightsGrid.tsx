import "devextreme/dist/css/dx.light.css";

import "./Flights.css";

import {
  Column,
  ColumnChooser,
  ColumnFixing,
  DataGrid,
  FilterRow,
  Item,
  SearchPanel,
  Selection,
  Toolbar,
} from "devextreme-react/data-grid";

import Occupation from "./Occupation";
import { Flight } from "../models/Flight";
import { EventInfo } from "devextreme/events";
import dxDataGrid, { SelectionChangedInfo } from "devextreme/ui/data_grid";
import { memo } from "react";

export const FlightsGrid = memo(({
  flights,
  onFlightselected,
}: {
  flights: Flight[] | undefined;
  //TODO : move to a separate file and rename this type or other types asm well | keep any
  onFlightselected: ((e: EventInfo<dxDataGrid<Flight, any>> & SelectionChangedInfo<Flight, any>) => void) | undefined;
}) => {
  console.log('rendering FlightsGrid')
  return (
    <DataGrid
      className="flight-grid"
      id="dataGrid"
      dataSource={flights} //this is where the data comes from
      keyExpr="FN" //???
      onSelectionChanged={onFlightselected}
      height={"100%"}
      columnAutoWidth={true}
      showColumnLines={true}
      showRowLines={true}
      showBorders={true}
      rowAlternationEnabled={true}
    >
      <ColumnChooser enabled={true} />
      <Column
        dataField="FN"
        allowSorting={false}
        dataType=""
        caption={`Flight Number`}
        //width={50}
      />
      <Column
        dataField="FI"
        allowSorting={false}
        dataType=""
        caption={`Flight Itinerary`}

        //width={50}
      />
      <Column
        dataField="ocupation"
        allowSorting={false}
        dataType=""
        caption={`Booked`}
        cellRender={(data) => {
          //console.log(`--------->${data}`);
          return <Occupation percent={Math.random()} />;
        }}
        //width={50}
      />
      <ColumnFixing enabled={true} />
      <FilterRow visible={true} />
      <SearchPanel visible={true} />
      <Selection mode="single" />
      <Toolbar>
        <Item name="groupPanel" />
        <Item name="exportButton" />
        <Item location="before" name="searchPanel" />
      </Toolbar>
    </DataGrid>
  );
});
