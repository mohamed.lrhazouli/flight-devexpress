import "devextreme/dist/css/dx.light.css";
import { memo, useCallback, useState } from "react";
import "./Flights.css";

import { Flight } from "../models/Flight";
import ContingentsGrid from "./ContingentsGrid";

import { FlightsGrid } from "./FlightsGrid";
export const Flights = ({ flights }: { flights: Flight[]  }) => {
  const [selectedFlight, setSelectedFlight] = useState<Flight>();
  const onFlightselected = useCallback((e: any) => {
    e.component.byKey(e.currentSelectedRowKeys[0]).done((flight: Flight) => {
      //console.log(flight);
      setSelectedFlight(flight);
    });
  }, [])
  
  return (
    <div className="flight-list">
      <div className="left-container">
        <FlightsGrid flights={flights} onFlightselected={onFlightselected} />
      </div>
      <div></div>
      <div className="right-container">
        <ContingentsGrid flight={selectedFlight} />
      </div>
    </div>
  );
};
