import { ProgressBar } from "devextreme-react/progress-bar";
/**
 *
 *
 */
const Occupation = ({ percent }: { percent: number }) => {
  const colorsRules = {
    ranges: [
      { startValue: 0, endValue: 20, color: "midnightblue" },
      { startValue: 20, endValue: 50, color: "red" },
      { startValue: 50, endValue: 70, color: "green" },
      // ...
    ],
  };
  function calculateColor(value: number) {}
  function statusFormat(value: number) {
    //TODO if you try this with value with 0.56 for example it gives you 56.00000000000001%
    //=> limit number of digits using a float method

    return `${`${value * 100}`.slice(0, 5)}%  `;
  }
  return (
    <ProgressBar
      showStatus={true}
      id="progress-bar-status"
      className={percent >= 0.7 ? "green" : percent >= 0.5 ? "yellow" : "red"}
      width="100%"
      min={0}
      max={1}
      statusFormat={statusFormat}
      value={percent}
    />
  );
};

export default Occupation;
