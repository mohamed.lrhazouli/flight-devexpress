import { Link } from "react-router-dom";

export function Layout({ children }: { children: React.ReactNode }) {
  return (
    <>
      <nav>
        <h2>Menu</h2>
        <ul>
        <li>
            <Link to="/settigns">Settings</Link>
          </li>
          <li>
            <Link to="/flights">FLights</Link>
          </li>
        </ul>
      </nav>
      {children}
    </>
  );
}
